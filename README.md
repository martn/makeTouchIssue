# `make --touch` issue with out-of-tree Makefile strategy

We use the [out-of-tree compilation strategy](http://make.mad-scientist.net/papers/multi-architecture-builds/#advanced)
described by [Paul D. Smith](http://mad-scientist.net/).

However it seems it interfere with the `--touch` feature of `make`.

In the simplified `make` scenario, we are building a program `pgm.x` that 
depends on 2 objects: `file1.o` and `file2.o` in which the later use a function
contained in `file1.o`; this is why the `Makefile` have:
```make
file2.o: file1.o
```

## Problematic usecase
Let say, after a full build
```
$ make
compiling file1.o
compiling file2.o
compiling pgm.o
linking pgm.x
:
$ tree build/
build/
├── file1.o
├── file2.o
├── pgm.o
└── pgm.x

0 directories, 4 files
$ make 
make[1]: Nothing to be done for 'all'.
:
```
A developper changes the content of a function in `src/file1.f90` in a
way that **does not modify the function interface** and recompile the object:
```
$ touch src/file1.f90 # modifying func1 implementation
$ make file1.o
compiling file1.o
:
```

Although `file1.o` needed to be recompiled, neither `file2.o` nor
`pgm.o` need to be recompiled.
Obviously, `make` can't know that:
```
$ make -n 
[ -d build ] || mkdir -p build
make --no-print-directory -C build -f .../Makefile SRC_ROOT=... 
echo "compiling file2.o"
cp .../src/file2.f90 file2.o
echo "compiling pgm.o"
cp .../src/pgm.f90 pgm.o
echo "linking pgm.x"
cp pgm.o pgm.x
:
```
`make` would recompile both `file2.o`, `pgm.o` and relink `pgm.x`.
But in this case it could only relink.

So we use the `--touch` feature to indicate that both `file2.o` and `pgm.o` 
need no recompilation:
```
$ make --touch file2.o pgm.o 
touch file2.o
touch pgm.o
touch file2.o
touch pgm.o
```
Here is the first indication of the potential issue: the repetition of `touch`.
And we see what happened:
```
$ ls
Makefile  README.md  build  file2.o  pgm.o  src  target.mk
```
It did touch both `build/file2.o` and `build/pgm.o` since now `make -n`
indicates it would only proceed to linking
```
$  make -n
[ -d build ] || mkdir -p build
make --no-print-directory -C build -f /fs/homeu1/eccc/aq/arqi/mad001/code/makeExpProjects/makeTouchBug/Makefile SRC_ROOT=/fs/homeu1/eccc/aq/arqi/mad001/code/makeExpProjects/makeTouchBug 
echo "linking pgm.x"
cp pgm.o pgm.x
:
```
but it also touched two unwanted files in the project root directory.


Is there a way to prevent that?
