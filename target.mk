DIR_TARGET := $(DIR_BLD)


MAKETARGET = $(MAKE) --no-print-directory -C $@ -f $(CURDIR)/Makefile \
				SRC_ROOT=$(CURDIR) $(MAKECMDGOALS)


.PHONY: $(DIR_TARGET)

$(DIR_TARGET):
	+@[ -d $@ ] || mkdir -p $@
	+@$(MAKETARGET)

Makefile : ;

%.mk :: ;

% :: $(DIR_TARGET) ; :

# vim: set noexpandtab noautoindent:
