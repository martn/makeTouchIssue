DIR_SRC_REL := src
DIR_BLD := build


all: 


clean: 
	rm -rf $(DIR_BLD)

##===================================================================
##
## 	Multi-architecture multi-directories strategy
##	(see http://make.mad-scientist.net/papers/multi-architecture-builds)
##
ifeq (,$(filter $(DIR_BLD)%,$(notdir $(CURDIR))))
include target.mk
else
##===================================================================
##
##	Main make rules
##
##	(only processed when make called from within the build dorectory
##	see ./target.mk)
##	

DIR_SRC = $(SRC_ROOT)/$(DIR_SRC_REL)

%.o: $(DIR_SRC)/%.f90
	@echo "compiling $@"
	@cp $< $@

%.x: %.o
	@echo "linking $@"
	@cp $^ $@

all: pgm.x

pgm.o: file1.o file2.o

file2.o: file1.o

##===================================================================
endif
# vim: set noexpandtab noautoindent:
